"""
Filename: wordcount.py
Purpose: Counting words of a given file.
Author: O.O
"""

import sys
import re

def get_words(filename):
    """
    Finds all words inside a file.
    :param filename: The file path to look in.
    :return: A tuple containing:
        0. A list containing each word in the file (only 1 appearance per word) in lowercase
        1. A list containing each word in the file with multiple appearances per word
    """
    with open(filename, 'r') as read_file:
        content = read_file.read()

    words = re.findall(r'[a-zA-Z\']+', content)
    words = [word.lower() for word in words]

    isolated_words = list(set(words))
    isolated_words.remove("'")

    return words, isolated_words

def print_words(filename, order=None):
    """
    Prints all words in the file along with their count, sorted alphabetically
    :param filename: The file path to look in.
    :param order: The order in which to display the words.
    Order can be either alphabetical or by most appearances.
    """

    words, isolated_words = get_words(filename)

    if (order == "appearance") or (order == "letter"):
        isolated_words.sort()
        if order == "appearance":
            isolated_words.sort(key=words.count, reverse=True)
    elif order:
        print("Invalid option after count!")
        return

    for word in isolated_words:
        print(f"{word}: {words.count(word)}")


def print_top(filename):
    """
    Prins the 20 most frequent words in the file.
    :param filename: The file path to look in.
    """
    words, isolated_words = get_words(filename)

    isolated_words.sort(key=words.count, reverse=True)

    for index in range(20):
        print(f"{isolated_words[index]}: {words.count(isolated_words[index])}")


def main():
    if len(sys.argv) == 4:
        option = sys.argv[1]
        if option != '--count':
            print("usage: ./wordcount.py {--count [--letter | --appearance] | --topcount} file")
            sys.exit(1)
        order = sys.argv[2][2:]
        filename = sys.argv[3]
        print_words(filename, order)

    elif len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count [--letter | --appearance] | --topcount} file")

    else:
        option = sys.argv[1]
        filename = sys.argv[2]
        if option == '--count':
            print_words(filename)
        elif option == '--topcount':
            print_top(filename)
        else:
            print("unknown option: " + option)
            sys.exit(1)


if __name__ == '__main__':
    main()
